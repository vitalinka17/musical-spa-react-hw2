export const setLocalItem = (name, value) => {
  let existingEntries = JSON.parse(getLocalItem(name));
  existingEntries === null && (existingEntries = []);
  let entryItem = value;
  localStorage.setItem("item", entryItem);
  existingEntries.push(entryItem);
  let set = Array.from(new Set(existingEntries));
  localStorage.setItem(name, JSON.stringify(set));
};
export const getLocalItem = (name) => {
  return localStorage.getItem(name);
};

export const getNumberOfItems = (name) => {
  const arrayLength = JSON.parse(getLocalItem(name));
  if (!arrayLength) {
    return 0;
  } else {
    return arrayLength.length;
  }
};
