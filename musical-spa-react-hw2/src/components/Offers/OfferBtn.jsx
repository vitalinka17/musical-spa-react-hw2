import { Component } from "react";
import "./OfferBtn.scss";

class OfferBtn extends Component {
  render() {
    const { icon, title, description, color } = this.props;
    return (
      <div className={"btn__container"} style={{ background: color }}>
        <div className="title__btn__cont">
          <img src={icon} alt="icon" className="icon" />
          <p className="btn__title">{title}</p>
        </div>
        <p className="description">{description}</p>
      </div>
    );
  }
}
export default OfferBtn;
