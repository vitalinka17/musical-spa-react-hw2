import React, { Component, createRef } from "react";
import "./Modal.scss";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.myRef = createRef();
  }
  outsideCloseHandler = (e) => {
    if (e.target.contains(this.myRef.current)) {
      this.props.onClose();
    }
  };
  render() {
    const { header, status, onClose, text, onSubmit } = this.props;
    return (
      <div className="modal__wrapper" onClick={this.outsideCloseHandler}>
        <div ref={this.myRef} className={"modal"}>
          <div className="modal__header">
            <span className="modal__header__title">{header}</span>
            <span className="modal__header__close__btn" onClick={onClose}>
              X
            </span>
          </div>
          <p className="modal__message">{text}</p>
          <div className="modal__btn__container">
            {status === "CART" && (
              <button className="modal__btn agree__btn" onClick={onSubmit}>
                Ok
              </button>
            )}
            {status === "DELETE" && (
              <button className="modal__btn agree__btn">Ok</button>
            )}
            <button className="modal__btn cancel__btn">No</button>
          </div>
        </div>
      </div>
    );
  }
}
export default Modal;
