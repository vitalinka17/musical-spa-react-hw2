import { Component } from "react";
import "./SliderArrows.scss";
class SliderArrows extends Component {
  render() {
    return (
      <div className="slider__btn__container">
        <span className="left__arrow">&#11013;</span>
        <span className="right__arrow">&#10145;</span>
      </div>
    );
  }
}
export default SliderArrows;
