import { Component } from "react";
import "./Title.scss";
import { SliderArrows } from "./components/SliderArrows";
class Title extends Component {
  render() {
    const { text } = this.props;
    return (
      <div className="block__title container">
        <h4 className={"block__title__text"}>{text}</h4>
        <SliderArrows />
      </div>
    );
  }
}
export default Title;
