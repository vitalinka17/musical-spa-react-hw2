import { Component } from "react";
import "./Products.scss";
import { Title } from "../Title";
import { Card } from "../Card";
class Products extends Component {
  render() {
    const { data, titleText, sale, onPress, likeUpdate } = this.props;
    return (
      <div className={"products__wrapper container"}>
        <Title text={titleText} />
        <Card
          onPress={onPress}
          data={data}
          sale={sale}
          likeUpdate={likeUpdate}
        />
      </div>
    );
  }
}
export default Products;
