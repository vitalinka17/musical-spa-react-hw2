import { Component } from "react";
import "./Presentation.scss";
import { RightBtn, LeftBtn } from "./components/buttons";
import Title from "./components/Title/Title";
import mainBanner from "../../assets/img/presentation_photo6.jpeg";
import photo2 from "../../assets/img/presentation_photo2.jpeg";
import photo3 from "../../assets/img/presentation_photo3.jpeg";
import photo4 from "../../assets/img/presentation_photo4.jpeg";
import photo5 from "../../assets/img/presentation_photo5.jpeg";
import photo6 from "../../assets/img/main_banner.jpeg";

class Presentation extends Component {
  state = {
    mainPhoto: 0,
    photos: [mainBanner, photo2, photo3, photo4, photo5, photo6],
  };
  leftBtnHandler = () => {
    const { mainPhoto } = this.state;
    this.setState((prevState) =>
      mainPhoto <= 0
        ? { ...prevState, mainPhoto: prevState.photos.length - 1 }
        : { ...prevState, mainPhoto: mainPhoto - 1 }
    );
  };
  rightBtnHandler = () => {
    const { mainPhoto } = this.state;
    this.setState((prevState) =>
      mainPhoto === prevState.photos.length - 1
        ? { ...prevState, mainPhoto: 0 }
        : { ...prevState, mainPhoto: mainPhoto + 1 }
    );
  };
  render() {
    const { photos, mainPhoto } = this.state;
    return (
      <div className="presentation__wrapper">
        <div className="photo__container">
          <div className={"photo__wrapper"}>
            <img className="banner" src={photos[mainPhoto]} alt="photo" />
          </div>
        </div>
        <LeftBtn
          previous={this.leftBtnHandler}
          text={<span className="arrow">&#8249;</span>}
        />
        <RightBtn
          next={this.rightBtnHandler}
          text={<span className="arrow">&#8250;</span>}
        />
        <Title />
      </div>
    );
  }
}
export default Presentation;
