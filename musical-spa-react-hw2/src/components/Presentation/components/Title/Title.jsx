import { Component } from "react";
import "./Title.scss";
class Title extends Component {
  render() {
    return (
      <div className={"title__wrapper container"}>
        <div className="title__cont">
          <h2 className={"title"}>
            Welcome to <span className={"title__red"}>musica,</span> checkout
            out our latest albums
          </h2>
        </div>
      </div>
    );
  }
}
export default Title;
