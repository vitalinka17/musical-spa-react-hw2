import { Component } from "react";
import Buttons from "./Buttons.scss";
class LeftBtn extends Component {
  render() {
    const { previous, text } = this.props;
    return (
      <div onClick={previous} className={"circle left"}>
        {text}
      </div>
    );
  }
}
export default LeftBtn;
