import { Component } from "react";
import Buttons from "./Buttons.scss";
class RightBtn extends Component {
  render() {
    const { next, text } = this.props;
    return (
      <div onClick={next} className={"circle right"}>
        {text}
      </div>
    );
  }
}
export default RightBtn;
