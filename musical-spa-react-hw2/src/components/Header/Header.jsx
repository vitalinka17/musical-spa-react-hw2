import { Component } from "react";
import "./Header.scss";
import { SnsBar } from "./components/SnsBar";
import { Cart } from "./components/Cart";
import { NavBar } from "./components/NavBar";
import logoIcon from "../../assets/img/vinyl.png";
class Header extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { products, onPress } = this.props;
    return (
      <section className={"header__wrapper"}>
        <div className="top__header__wrapper container">
          <SnsBar />
          <div className="login__wrapper">
            <div className="labels__wrapper">
              <a href="#" className="login__labels">
                Login
              </a>
              <span className={"login__labels"}> / </span>
              <a href="#" className="login__labels">
                Register
              </a>
            </div>
            <Cart onPress={onPress} products={products} />
          </div>
        </div>
        <div className="bottom__header__wrapper container">
          <div className="brand__logo__cont">
            <a href="#" className="logo">
              <span className="brand__part">M</span>
              <img src={logoIcon} alt="logoIcon" className={"logo__icon"} />
            </a>
            <a href="#" className="brand__name">
              Store
            </a>
          </div>
          <NavBar />
        </div>
      </section>
    );
  }
}
export default Header;
