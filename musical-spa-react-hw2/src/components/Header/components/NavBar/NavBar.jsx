import { Component } from "react";
import "./NavBar.scss";
class NavBar extends Component {
  render() {
    return (
      <nav className="navbar">
        <ul className="menu">
          <li className="menu__item">
            <a className={"menu__item__link"} href="#">
              Home
            </a>
          </li>
          <li className="menu__item">
            <a className={"menu__item__link"} href="#">
              CD's
            </a>
          </li>
          <li className="menu__item">
            <a className={"menu__item__link"} href="#">
              Artists
            </a>
          </li>
          <li className="menu__item">
            <a className={"menu__item__link"} href="#">
              Bestsellers
            </a>
          </li>
          <li className="menu__item">
            <a className={"menu__item__link"} href="#">
              About us
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}
export default NavBar;
