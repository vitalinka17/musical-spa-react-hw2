import { Component } from "react";
import "./Cart.scss";
import { OpenedCart } from "./components";
import cart from "../../../../assets/img/cart-icon.png";
import { getNumberOfItems } from "../../../../utils";

class Cart extends Component {
  state = {
    isOpenCart: false,
  };
  getCounterValue = () => {
    return getNumberOfItems("cards");
  };
  render() {
    const { isOpenCart } = this.state;
    const { products, onPress } = this.props;
    return (
      <div
        className="cart"
        onClick={() => {
          this.setState((prevState) => {
            return { ...prevState, isOpenCart: !isOpenCart };
          });
        }}
      >
        <div className={"cart__container"}>
          <div className="cart__image__cont">
            <img className={"cart__image"} src={cart} alt="cart-image" />
          </div>
          <div className="cart__label">
            Cart{" "}
            <span className={"cart__counter"}>({this.getCounterValue()})</span>
          </div>
        </div>
        {isOpenCart && <OpenedCart onPress={onPress} data={products} />}
      </div>
    );
  }
}
export default Cart;
