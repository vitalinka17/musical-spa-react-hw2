import { Component } from "react";
import "./OpenedCart.scss";
import Button from "./buttons/Button";
import RatingStar from "../../../../RatingStar/RatingStar";
import { getLocalItem } from "../../../../../utils";
import { type } from "@testing-library/user-event/dist/type";

class OpenedCart extends Component {
  counterCartSum = (info) => {
    const mapped = info().map(({ price, newPrice }) => {
      if (price) {
        return Number(price);
      }
      if (newPrice) {
        return Number(newPrice);
      }
    });
    let sum = mapped.reduce((partialSum, a) => partialSum + a, 0);
    const sumRound = sum.toFixed(2);
    return sumRound;
  };
  render() {
    const info = this.props.data;
    // const { onPress } = this.props;
    return (
      <div className="opened__cart">
        {info().map(
          ({ title, artist, rating, price, banner, id, newPrice }) => {
            return (
              <div
                key={id}
                id={`product_${id}`}
                className="cart__product__container"
              >
                <img src={banner} alt={title} className="product__icon" />
                <div className="purchase__info">
                  <span className="quantity">1</span>
                  <span className="multiply"> x </span>
                  <span className="product__title">{title}</span>
                  <span className="product__artist"> by {artist}</span>
                  <RatingStar rating={rating} />
                </div>
                <div className="price__cont">
                  {newPrice && <p className="price"> {newPrice} </p>}
                  {price && <span className="price"> {price}</span>}
                  <div
                    className="delete__btn"
                    onClick={() => {
                      const value = Number(id);
                      let parsed = JSON.parse(getLocalItem("cards"));
                      parsed = parsed.filter((item) => Number(item) !== value);
                      localStorage.setItem("cards", JSON.stringify(parsed));
                    }}
                  >
                    x
                  </div>
                </div>
              </div>
            );
          }
        )}
        <div className="total__cost__container">
          <span className="total__cost__info">Total cost with delivery:</span>
          <span className="total__cost">{this.counterCartSum(info)}</span>
        </div>
        <div className="btn__container">
          <Button text={"View Cart"} />
          <Button text={"Proceed to Checkout"} />
        </div>
      </div>
    );
  }
}
export default OpenedCart;
