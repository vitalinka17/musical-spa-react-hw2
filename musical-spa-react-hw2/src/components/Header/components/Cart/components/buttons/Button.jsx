import { Component } from "react";
import "./Button.scss";
class Button extends Component {
  render() {
    const { text } = this.props;
    return (
      <button className={"cart__btn"} type="button">
        {text}
      </button>
    );
  }
}
export default Button;
