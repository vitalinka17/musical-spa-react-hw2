import { Component } from "react";
import "./SnsBar.scss";
import fbLogo from "./assets/fb-logo.jpeg";
import igLogo from "./assets/ig-logo.jpeg";
import twitterLogo from "./assets/twitter-logo.png";
import dropboxLogo from "./assets/dropbox-logo.png";
import vimeoLogo from "./assets/vimeo-logo.png";
import emailLogo from "./assets/email-logo.png";

class SnsBar extends Component {
  render() {
    return (
      <div className={"sns__bar"}>
        <a className={"sns__bar__image__container"} href="#">
          <img className={"sns__bar__image"} src={fbLogo} alt="fb-logo" />
        </a>
        <a className={"sns__bar__image__container"} href="#">
          <img className={"sns__bar__image"} src={igLogo} alt="igLogo" />
        </a>
        <a className={"sns__bar__image__container"} href="#">
          <img
            className={"sns__bar__image"}
            src={twitterLogo}
            alt="twitterLogo"
          />
        </a>
        <a className={"sns__bar__image__container"} href="#">
          <img
            className={"sns__bar__image"}
            src={dropboxLogo}
            alt="dropboxLogo"
          />
        </a>
        <a className={"sns__bar__image__container"} href="#">
          <img className={"sns__bar__image"} src={vimeoLogo} alt="vimeoLogo" />
        </a>
        <a className={"sns__bar__image__container"} href="#">
          <img className={"sns__bar__image"} src={emailLogo} alt="emailLogo" />
        </a>
      </div>
    );
  }
}
export default SnsBar;
