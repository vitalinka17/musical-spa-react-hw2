import { Component } from "react";
import "./Card.scss";
import AddToCartBtn from "./components/AddToCartBtn/AddToCartBtn";
import RatingStar from "../RatingStar/RatingStar";
import SaleFlag from "./components/SaleFlag/SaleFlag";
import { HeartIcon } from "./components/icons/HeartIcon";
import { getLocalItem } from "../../utils";

class Card extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { data, sale, onPress, likeUpdate } = this.props;
    return (
      <div className="cards__bar">
        {data.map(
          (
            {
              title,
              artist,
              rating,
              price,
              banner,
              id,
              description,
              oldPrice,
              newPrice,
              liked,
            },
            index
          ) => {
            return (
              <div key={index} id={id} className={"card"}>
                <div className="song__banner__wrapper">
                  {sale && <SaleFlag />}
                  <img src={banner} alt={title} />
                </div>
                <div className="detailed__song__info">
                  <span className="song__title">{title} </span>
                  <span className="song__artist"> by {artist} </span>
                  <RatingStar rating={rating} />
                  <HeartIcon onPress={() => likeUpdate(id)} liked={liked} />
                  <p className="song__description">{description} </p>
                  {oldPrice && (
                    <p className="song__price--old">$ {oldPrice} </p>
                  )}
                  {newPrice && (
                    <p className="song__price--new">$ {newPrice} </p>
                  )}
                  {price && <p className="song__price">$ {price} </p>}
                </div>
                <AddToCartBtn id={id} onPress={onPress} />
              </div>
            );
          }
        )}
      </div>
    );
  }
}
export default Card;
