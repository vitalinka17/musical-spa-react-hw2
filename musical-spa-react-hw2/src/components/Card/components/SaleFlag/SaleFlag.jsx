import { Component } from "react";
import "./SaleFlag.scss";
class SaleFlag extends Component {
  render() {
    return (
      <span className={"sale__circle circle"}>
        <span className={"sale__text"}>Sale</span>
      </span>
    );
  }
}
export default SaleFlag;
