import { Component } from "react";
import { ReactComponent as FavoriteHeart } from "./Favourite.svg";
export class HeartIcon extends Component {
  state = {
    colors: ["black", "red"],
  };

  render() {
    const [notClicked, clicked] = this.state.colors;
    const { onPress, liked, id } = this.props;
    return (
      <div
        onClick={onPress}
        style={{
          width: "32px",
          height: "32px",
          position: "absolute",
          top: "62%",
          left: "82%",
          fill: notClicked,
          cursor: "pointer",
          ...(liked ? { fill: clicked } : { fill: notClicked }),
        }}
      >
        <FavoriteHeart />
      </div>
    );
  }
}
