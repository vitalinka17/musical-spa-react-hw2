import { Component } from "react";
import "./AddToCart.scss";

class AddToCart extends Component {
  render() {
    const { onPress, id } = this.props;
    return (
      <button
        className={"add__to__cart--btn"}
        type={"button"}
        onClick={() => {
          onPress(id);
        }}
      >
        Add to Cart
      </button>
    );
  }
}
export default AddToCart;
