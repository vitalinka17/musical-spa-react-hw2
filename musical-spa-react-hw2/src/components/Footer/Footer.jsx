import { Component } from "react";
import "./Footer.scss";
import { SnsBar } from "../Header/components/SnsBar";
import Album1 from "../../assets/img/album1.jpeg";
import Album2 from "../../assets/img/album2.jpeg";
import CommentIcon from "../../assets/img/comment.png";
class Footer extends Component {
  render() {
    return (
      <footer className={"footer container"}>
        <div className="company__info">
          <p className="company__info__title">Little about us</p>
          <p className="company__info__description">
            Стримінговий сервіс потокового аудіо, що дозволяє прослуховувати
            музичні композиції та подкасти.
          </p>
          <p className="company__info__title">Keep in touch with us</p>
          <SnsBar />
        </div>
        <div className="archives">
          <p className="company__info__title">Our archives</p>
          <ul className="archive__list">
            <li className="archive__list__item">March 2021</li>
            <li className="archive__list__item">February 2021</li>
            <li className="archive__list__item">January 2021</li>
            <li className="archive__list__item">December 2021</li>
          </ul>
        </div>
        <div className="posts">
          <p className="company__info__title">Our albums</p>
          <ul className="albums__list">
            <li className="albums__list__item">
              <img src={Album1} alt="Album1" className="album__poster" />
              <p className="album__name">Great album</p>
              <span className="comments__counter">&#128173; 12 comments</span>
            </li>
            <li className="albums__list__item">
              <img src={Album2} alt="Album2" className="album__poster" />
              <p className="album__name">Great album</p>
              <span className="comments__counter">&#128173; 12 comments</span>
            </li>
          </ul>
        </div>
        <div className="site__navigation">
          <div className="search">
            <p className="company__info__title">Search on our site</p>
            <input
              type="text"
              className={"search__input"}
              placeholder={"You are looking for..."}
            />
          </div>
          <div className="tag__area">
            <p className="company__info__title">Tag Cloud</p>
            <div className="tag__container">
              <span className="tag">Audio CD</span>
              <span className="tag">Video</span>
              <span className="tag">Playlist</span>
            </div>
          </div>
        </div>
        <div className="footer__nav">
          <div className="route__wrapper">
            <a href="" className="link__item">
              Home
            </a>
            <a href="" className="link__item">
              Portfolio
            </a>
            <a href="" className="link__item">
              Sitemap
            </a>
            <a href="" className="link__item">
              Contact
            </a>
          </div>
          <div className="license">
            <span className="developers">
              Musica &copy; 2022 by DanIT Coding
            </span>
            <span className={"divider"}> | </span>
            <span className={"rights"}>All Rights Reserved</span>
          </div>
        </div>
      </footer>
    );
  }
}
export default Footer;
