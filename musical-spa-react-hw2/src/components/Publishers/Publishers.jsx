import { Component } from "react";
import "./Publishers.scss";
import AmazonLogo from "../../assets/img/amazonLogo.jpeg";
import ItunesLogo from "../../assets/img/itunesLogo.jpeg";
import NetflixLogo from "../../assets/img/netflixLogo.png";
import SoundcloudLogo from "../../assets/img/soundcliudLogo.png";
import SpotifyLogo from "../../assets/img/SpotifyLogo.png";
import YouTubeMusicLogo from "../../assets/img/youtubeMusicLogo.png";
class Publishers extends Component {
  state = {
    logos: [
      AmazonLogo,
      ItunesLogo,
      NetflixLogo,
      SoundcloudLogo,
      SpotifyLogo,
      YouTubeMusicLogo,
    ],
  };
  render() {
    const { logos } = this.state;
    return (
      <div className={"publishers__wrapper container"}>
        <h3 className={"publishers__title container"}>
          Our most popular publishers
        </h3>
        <div className="logo__container container">
          {logos.map((item, index) => {
            return (
              <div key={index} className="logo__box">
                <img src={item} alt="logo" className="logo" />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
export default Publishers;
