import React, { Component } from "react";
import "./App.scss";
import { Header } from "./components/Header";
import Presentation from "./components/Presentation";
import { Products } from "./components/Products";
import OfferBtn from "./components/Offers";
import { Publishers } from "./components/Publishers";
import { Modal } from "./components/Modal";
import { Footer } from "./components/Footer";
import CalendarIcon from "./assets/img/calendarIcon.png";
import HeadphonesIcon from "./assets/img/headphonesIcon.png";
import CDIcon from "./assets/img/CDicon.png";
import { getLocalItem, setLocalItem } from "./utils";

class App extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    cardId: null,
    error: null,
    isLoaded: false,
    items: [],
    isSale: false,
    modal: null,
  };
  componentDidMount() {
    fetch("API/data.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState((prevState) => {
            return {
              ...prevState,
              isLoaded: !prevState.isLoaded,
              items: result,
            };
          });
        },
        (error) => {
          this.setState((prevState) => {
            return {
              ...prevState,
              isLoaded: !prevState.isLoaded,
              error,
            };
          });
        }
      );
  }
  actionModalHandler = (status) => {
    this.setState({
      modal: status,
    });
  };
  getItems = () => {
    const { items } = this.state;
    const parsed = JSON.parse(getLocalItem("cards"));
    if (!parsed) return [];
    return items
      .map((item) => {
        if (parsed.includes(item.id)) return item;
      })
      .filter(Boolean);
  };

  render() {
    const { items, isSale, modal } = this.state;
    const modals = {
      DELETE: (
        <Modal
          status={"DELETE"}
          header={"Do you want to delete this song ?"}
          text={
            <span>
              If you delete this song, it'll disappear from your cart <br />
              Are you sure you want to delete it?
            </span>
          }
          closeButton={true}
          onClose={() => {
            this.actionModalHandler(null);
          }}
        />
      ),
      CART: (
        <Modal
          status={"CART"}
          header={"Do you want to add this item to your cart?"}
          text={
            <span>
              We appreciate your choice <br />
              Do you confirm adding this song to your cart?
            </span>
          }
          onSubmit={() => {
            setLocalItem("cards", this.state.cardId);
            this.actionModalHandler(null);
          }}
          onClose={() => {
            this.actionModalHandler(null);
          }}
        />
      ),
    };
    const activeModal = modals[modal] ?? null;
    return (
      <div>
        {activeModal}
        <Header
          onPress={() => {
            this.actionModalHandler("DELETE");
          }}
          products={this.getItems}
        />
        <Presentation />
        <div className="btn__wrapper container">
          <OfferBtn
            color={"purple"}
            icon={CDIcon}
            title={"Check our CD collection"}
            description={
              "Enjoy headliners' hits and best records from all over the world"
            }
          />
          <OfferBtn
            color={"grey"}
            icon={HeadphonesIcon}
            title={"Listen before purchase"}
            description={
              "You can listen to the songs before buying, you have a 1-day trial period"
            }
          />
          <OfferBtn
            color={"red"}
            icon={CalendarIcon}
            title={"Upcoming events"}
            description={
              "We have a high-quality newsletter service that will keep noted about the events on the schedule"
            }
          />
        </div>
        <Products
          likeUpdate={(id) => {
            this.setState((prevState) => {
              return {
                ...prevState,
                items: prevState.items.map((item) => {
                  if (item.id === id) {
                    setLocalItem("favourite", item.id);
                    return {
                      ...item,
                      liked: !item.liked,
                    };
                  }
                  return item;
                }),
              };
            });
          }}
          data={items}
          titleText={"Latest arrivals at Musica"}
          sale={isSale}
          onPress={(id) => {
            this.setState((prevState) => ({
              ...prevState,
              cardId: id,
            }));
            this.actionModalHandler("CART");
          }}
        />
        <Products
          likeUpdate={(id) => {
            this.setState((prevState) => {
              return {
                ...prevState,
                items: prevState.items.map((item) => {
                  if (item.id === id) {
                    setLocalItem("favourite", item.id);
                    return {
                      ...item,
                      liked: !item.liked,
                    };
                  }
                  return item;
                }),
              };
            });
          }}
          data={items.filter((item) => {
            if (item.sale) return item;
          })}
          titleText={"Hits currently on sale"}
          sale={() => {
            this.setState((prevState) => ({
              isSale: !prevState.isSale,
            }));
          }}
          onPress={(id) => {
            this.setState((prevState) => ({
              ...prevState,
              cardId: id,
            }));
            this.actionModalHandler("CART");
          }}
        />
        <Publishers />
        <Footer />
      </div>
    );
  }
}

// items: prevState.items.map((item) => {
//   if (item.id === id) {
//     if (!item.liked) {
//       const value = Number(item.id);
//       console.log(value);
//       let parsed = JSON.parse(getLocalItem("favourite"));
//       if (!parsed) return [];
//       parsed = parsed.filter((item) => Number(item) !== value);
//       console.log("new", parsed);
//       localStorage.setItem("favourite", JSON.stringify(parsed));
//     }
//   }
// }),
export default App;
